/* Makes canvas go fullscreen */
const fullscreen = function () {
  const c = document.getElementById('canvas')
  if(!document.fullscreenElement)
    c.requestFullscreen()
};

/* On fullscreen change event updates clock's size */
const onFullscreenChange = function () {
  const c = document.getElementById('canvas')
  if (document.fullscreenElement) {
    c.setAttribute('height', '100%')
    c.setAttribute('width', '100%')
    const currHeight = window.innerHeight
    const currWidth = window.innerWidth
    // fit to screen with some padding
    const a = currHeight < currWidth ? currHeight : currWidth
    clock.setSize(a/2 * 1.05, a/2 * 1.05, a/2 * 0.8)
    clock.render()
  } else {
    c.setAttribute('width', '500')
    c.setAttribute('height', '500')
    clock.setSize(250, 250, 200)
  }
  clock.render()
};

// start app
const clock = new Clock();
clock.render();

// Adds listeners
document.getElementById("on_off").addEventListener('click', () => clock.switchTimer())
document.getElementById("current_time").addEventListener('click', () => clock.resetTime())
document.getElementById("show_hide_seconds").addEventListener('click', () => clock.switchSecondHandVisibility())
document.getElementById("relative-turn").addEventListener('change', () => clock.switchRelativeTurn())
document.getElementById('fullscreen-btn').addEventListener('click', fullscreen);
document.addEventListener('fullscreenchange', onFullscreenChange);

// add zooming functionality
// use d3 built-in zoom functionality
/*
let zoomCallback = (group) =>
    () => group.attr('transform', d3.event.transform);

let zoomBehaviour = d3.zoom()
    .scaleExtent([1, 20]) // min/max zoom level
    .on('zoom', zoomCallback(clock.clockGroup));

d3.select('#canvas')
    .call(zoomBehaviour);
*/