class Clock {
  constructor(x = 250, y = 250, r = 200) {
    this.hourHand = new Hand(this, 'hour');
    this.minuteHand = new Hand(this, 'minute');
    this.secondHand = new Hand(this, 'second');

    this.setSize(x, y, r)

    this.minuteFrame = new Frame(this, 'minute');
    this.hourFrame = new Frame(this, 'hour');
    this.timer = true;
    this.secondHandVisible = true;
    this.relativeTurn = false;
    this.resetTime()

    setInterval(this.moveTime.bind(this), Clock.TIME_DELTA)
  }

  moveTime() {
    if (this.timer) {
      this.currentTime.setTime(this.currentTime.getTime() + Clock.TIME_DELTA)
      this.setTimepartsFromTime()
    }
  }

  setSize(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;

    this.hourHand.setSize(r * 0.8, r * 0.1)
    this.minuteHand.setSize(r * 1.05, r * 0.05)
    this.secondHand.setSize(r * 1.2, 4)
  }

  switchTimer() {
    this.timer = !this.timer;
  }

  switchSecondHandVisibility() {
    this.secondHandVisible = !this.secondHandVisible;
  }

  switchRelativeTurn() {
    this.relativeTurn = !this.relativeTurn;
    this.setTimepartsFromTime() // uses "minutes" as origin
  }

  resetTime() {
    this.currentTime = new Date();
    this.setTimepartsFromTime()
  }

  getTimeData() {
    return [
      {
        "unit": "second",
        "value": this.second
      }, {
        "unit": "minute",
        "value": this.minute
      }, {
        "unit": "hour",
        "value": this.hour
      }
    ];
  }

  setTimeFromTimeparts() {
    this.currentTime.setHours(this.hour, this.minute)
  }

  setTimepartsFromTime() {
    this.second = this.currentTime.getSeconds();
    this.minute = this.currentTime.getMinutes() + this.second / 60; // make it smooth
    this.hour = this.currentTime.getHours() + this.minute / 60; // make it smooth
  }

  render() {
    d3.select('g').remove() // clear existing graphics

    this.clockGroup = d3.select('#canvas')
      .append('g')
      .attr('class', 'clock')
      .attr('transform', `translate(${this.x}, ${this.y})`);

    // draw clock frame
    /*
    this.clockGroup
      .append('circle')
      .attrs({
        r: this.r,
        fill: 'white',
        stroke: 'black',
        'stroke-width': 2,
        class: 'clock-frame'
      });
    */

    // draw hour hand
    this.hourHand.render();

    // draw minute hand
    this.minuteHand.render();

    this.minuteFrame.render();
    this.hourFrame.render();

    // draw second hand
    this.secondHand.render();

    // draw pin
    this.clockGroup
      .append('circle')
      .attrs({r: 4, fill: 'black', class: 'clock-pin'});

    // show time as text
    this.clockGroup
      .append('text')
      .attrs({'id': 'time', 'y': this.r + 30})
      .styles({'font-size': '30px'});

    // start render loop
    setInterval(() => {
      this.updateHands()
      this.updateTimeLabel()
    }, 100)
  }

  updateHands() {
    // draw hour hand
    this.hourHand.update();
    // draw minute hand
    this.minuteHand.update();
    // draw second hand
    this.secondHand.update();
  }

  updateTimeLabel() {
    let time = this.getTimeData();
    let hour = time.find(part => part.unit === 'hour');
    let minute = time.find(part => part.unit === 'minute');
    let second = time.find(part => part.unit === 'second');
    let sHour = _.padStart(Math.floor(hour.value), 2, '0');
    let sMinute = _.padStart(Math.floor(minute.value), 2, '0');
    let sSecond = _.padStart(second.value, 2, '0');
    this.clockGroup.select('#time')
      .text(`${sHour}:${sMinute}:${sSecond}`);
  }
}

Clock.TIME_DELTA = 100