class Hand {
  constructor(clock, className) {
    this.clock = clock;
    this.className = className;
  }

  setSize(length, width) {
    this.length = length;
    this.width = width;
  }

  setTimeFromRotation(x, y) {
    let rad = Math.atan2(x, y);
    let angle = -rad * (180 / Math.PI);
    // console.log("angle", angle);
    let scale = null;
    if (this.className === 'minute') {
      scale = d3.scaleLinear()
                .range([0, 60])
                .domain([-180, 180]);
      this.clock.minute = scale(angle);
      if (this.clock.relativeTurn)
        this.setHourRotationFromMinute(scale(angle))
    } else if (this.className === 'hour') {
      //value = value > 12 ? value - 12 : value;
      scale = d3.scaleLinear()
                .domain([-180, 180])
                .range([0, 12]);
      this.clock.hour = scale(angle);
      if (this.clock.relativeTurn)
        this.setMinuteRotationFromHour(scale(angle))
    }
    this.rotation = angle;
    this.clock.setTimeFromTimeparts()
    // console.log('hour', this.clock.hour, 'minute', this.clock.minute);
  }

  setRotationFromTime() {
    let timeData = this.clock.getTimeData();
    let timePart = timeData.find(part => part.unit === `${this.className}`);
    let value = timePart.value;
    let scale = null;
    if (this.className === 'second') {
      scale = d3.scaleLinear()
                .domain([0, 60]) // decimal scale
                .range([-180, 180]);
    } else if (this.className === 'minute') {
      scale = d3.scaleLinear()
                .domain([0, 60])
                .range([-180, 180]);
    } else if (this.className === 'hour') {
      value = value > 12 ? value - 12 : value;
      scale = d3.scaleLinear()
                .domain([0, 12])
                .range([-180, 180]);
    }
    this.rotation = scale(value);
  }

  setHourRotationFromMinute(minuteValue) {
    let hourValue = parseInt(this.clock.hour)
    let scale = d3.scaleLinear()
              .domain([0, 60])
              .range([0, 1]);
    this.clock.hour = hourValue + scale(minuteValue)
  }

  setMinuteRotationFromHour(hourValue) {
    hourValue %= 1 // use decimal part only
    let scale = d3.scaleLinear()
              .domain([0, 1])
              .range([0, 60]);
    this.clock.minute = scale(hourValue)
  }

  setRotation(rotation) {
    this.rotation = rotation
  }

  getRotation() {
    return this.rotation;
  }

  render() {
    this.setRotationFromTime();
    this.group = this.clock.clockGroup;

    // render hand
    let view = this.group
                   .append('polygon')
                   .attrs({
                     points: `0,0 ${this.width / 2},${this.length * 20 / 100} 0,${this.length} ${-this.width / 2},${this.length * 20 / 100}`,
                     class: `clock-hand ${this.className}`,
                     'transform': `translate(0 ${-this.length * 20 / 100}) rotate(${this.getRotation()} 0 ${this.length * 20 / 100})`
                   });
    // add drag listener
    let hand = this;
    let clock = this.clock;
    view.call(d3.drag()
                .on("start", function (d) {
                  view.classed('drag', true);
                  clock.timer = false;
                })
                .on("drag", function (d) {
                  let x = d3.event.x;
                  let y = d3.event.y;
                  hand.setTimeFromRotation(x, y);
                })
                .on("end", function () {
                  view.classed('drag', false);
                }));

    if (this.className === 'minute') {
      // render arcs
      this.group
          .append('path')
          .attr('id', `minute-filled-arc`)
        //.attr("d", arc)
          .styles({fill: 'green'});
      // render arc
      this.group
          .append('path')
          .attr('id', 'minute-empty-arc')
        //.attr("d", arc)
          .styles({fill: 'lightGray'});
    } else if (this.className === 'hour') {
      // render arcs
      this.group
          .append('path')
          .attr('id', `hour-filled-arc`)
        //.attr("d", arc)
          .styles({fill: 'blue'});
      // render arc
      this.group
          .append('path')
          .attr('id', 'hour-empty-arc')
        //.attr("d", arc)
          .styles({fill: 'lightGray'});
    }
  }

  update() {
    this.setRotationFromTime();

    // update hand
    this.group.select(`.${this.className}`)
        .attr('transform', `translate(0 ${-this.length * 20 / 100}) rotate(${this.getRotation()} 0 ${this.length * 20 / 100})`);
    this.group.select('.second').style('visibility', this.clock.secondHandVisible ? 'visible' : 'hidden') // second hand visibility

    // update arcs
    if (this.className === 'minute') {
      this._updateMinuteArc()
    } else if (this.className === 'hour') {
      this._updateHourArc()
    }
  }

  /* private */

  _updateMinuteArc() {
    let bgFilledArc = d3.arc()
                        .innerRadius(this.clock.r - 9)
                        .outerRadius(this.clock.r - 1)
                        .startAngle(0)
                        .endAngle((this.getRotation() + 180) * Math.PI / 180);
    this.group.select(`#minute-filled-arc`)
        .attr('d', bgFilledArc);

    let bgEmptyArc = d3.arc()
                       .innerRadius(this.clock.r - 9)
                       .outerRadius(this.clock.r - 1)
                       .startAngle((this.getRotation() + 180) * Math.PI / 180)
                       .endAngle(2 * Math.PI);
    this.group.select(`#minute-empty-arc`)
        .attr('d', bgEmptyArc);
  }

  _updateHourArc() {
    let bgFilledArc = d3.arc()
                        .innerRadius(this.clock.r - 23)
                        .outerRadius(this.clock.r - 15)
                        .startAngle(0)
                        .endAngle((this.getRotation() + 180) * Math.PI / 180);
    this.group.select(`#hour-filled-arc`)
        .attr('d', bgFilledArc);

    let bgEmptyArc = d3.arc()
                       .innerRadius(this.clock.r - 24)
                       .outerRadius(this.clock.r - 14)
                       .startAngle((this.getRotation() + 180) * Math.PI / 180)
                       .endAngle(2 * Math.PI);
    this.group.select(`#hour-empty-arc`)
        .attr('d', bgEmptyArc);
  }
}