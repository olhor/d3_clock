class Frame {
    constructor(clock, className) {
        this.clock = clock;
        this.className = className;
    }

    scale(val) {
        if (this.className === 'minute') {
            let scale = d3.scaleLinear()
                .domain([0, 59 + 59 / 60])
                .range([0, 2 * Math.PI]);
            return scale(val);
        } else if (this.className === 'hour') {
            let scale = d3.scaleLinear()
                .domain([0, 11 + 59 / 60])
                .range([0, 2 * Math.PI]);
            return scale(val);
        }
        return null;
    }

    render() {
        this.group = this.clock.clockGroup;
        if (this.className === 'minute') {
            for (let i = 0; i < 60; i++) {
                let arc = d3.arc()
                    .innerRadius(this.clock.r - 10)
                    .outerRadius(this.clock.r)
                    .startAngle(this.scale(i))
                    .endAngle(this.scale(i));
                this.group
                    .append('path')
                    .attr("d", arc)
                    .styles({stroke: 'white', 'stroke-width': 1});
            }
        } else if (this.className === 'hour') {
            for (let i = 0; i < 12; i++) {
                let arc = d3.arc()
                    .innerRadius(this.clock.r - 24)
                    .outerRadius(this.clock.r - 14)
                    .startAngle(this.scale(i))
                    .endAngle(this.scale(i));
                this.group
                    .append('path')
                    .attr("d", arc)
                    .attr('id', `hour-${i}`)
                    .styles({stroke: 'white', 'stroke-width': 1});

                let lbl = i === 0 ? 12 : i;
                let labelArc = d3.arc()
                    .innerRadius(this.clock.r-55)
                    .outerRadius(this.clock.r-55)
                    .startAngle(this.scale(i-0.5))
                    .endAngle(this.scale(i+0.5));
                this.group
                    .append('path')
                    .attr("d", labelArc)
                    .attr('id', `hour-label-${i}`)
                    //.attr('stroke', 'black');
                this.group
                    .append('text')
                    .attr('text-anchor', 'middle')
                    .style('font-size', '20px')
                    .append('textPath')
                    .attr('xlink:href', `#hour-label-${i}`)
                    .attr('startOffset', '25%')
                    .text(`${lbl}`);
            }
        }
    }
}